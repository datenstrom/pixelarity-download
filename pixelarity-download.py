#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import time
import shutil
import zipfile
import logging
import argparse
from getpass import getpass

import progressbar
from progressbar import ProgressBar
from bs4 import BeautifulSoup
from robobrowser import RoboBrowser

_logger = logging.getLogger(__name__)

def main(args):
    """Download all available templates from pixelarity.

    """
    setup_logs()
    args = parse_args(args)
    browser = RoboBrowser()
    pixelarity_login(browser)

    widgets = [
        'Progress: ', progressbar.Percentage(),
        ' ', progressbar.Bar(marker="=", left="[", right="]"),
        ' ', progressbar.ETA(),
        ' ', progressbar.FileTransferSpeed(),]

    templates = get_templates()
    with ProgressBar(widgets=widgets, max_value=len(templates)) as progress:

        prefix = args.prefix + "/"
        if not os.path.exists(prefix):
            os.makedirs(prefix)

        for template in templates:
            download_template(prefix, template, browser)
            progress += 1


def  pixelarity_login(browser):
    """Login to Pixelarity.

    """
    browser.open("https://pixelarity.com/login")
    login_form = browser.get_form(id="ajaxForm1")
    login_form["email"].value = input("Enter your Pixelarity username: ")
    login_form["password"].value = getpass("Enter your Pixelarity password: ")

    browser.submit_form(login_form)


def get_templates():
    """Builds a list of all web templates available for download.

    """
    templates = []

    browser = RoboBrowser()
    browser.open("https://pixelarity.com/")
    result = browser.parsed()
    soup = BeautifulSoup(str(result[0]), "html.parser")
    text = soup.find("section").find_all("article")

    for section in text:
        templates.append(section.a.get("href").replace("/", ""))

    templates = [item.lower() for item in templates]
    templates = [item.replace(" ", "") for item in templates]

    return templates


def download_template(prefix, template, browser):
    """Download a single template from Pixelarity.

    """
    template_root = "{}px-{}/".format(prefix, template)
    url_root = "https://pixelarity.com/" + template

    template_url = url_root + "/download/html"
    template_jekyll_url = url_root + "/download/jekyll"
    template_psd_url = url_root + "/download/psd"

    #  Create folder to store template
    if not os.path.exists(template_root):
        os.makedirs(template_root)


    zip_names = []
    dir_names = ['html', 'jekyll', 'psd']
    template_urls = [template_url, template_jekyll_url, template_psd_url]
    for name in dir_names:
        zip_names.append(template + '_' + name)

    for url, zip_name, dir_name in zip(template_urls, zip_names, dir_names):
        if not os.path.exists(template_root + zip_name + ".zip"):
            request = browser.session.get(url, stream=True)
            if request.status_code == 200:
                store_zip(template_root, zip_name, request, dir_name)
            else:
                if request.status_code == 429:
                    time.sleep(60)
                    request = browser.session.get(url, stream=True)
                    if request.status_code == 200:
                        store_zip(template_root, zip_name, request, dir_name)
                    else:
                        _logger.error("\nAborting on {} {} with second status code: {}"
                                      .format(template, dir_name, request.status_code))
                        sys.exit(1)
                elif request.status_code == 404 and dir_name in ['jekyll', 'psd']:
                    _logger.info("\nDownload for {} {} failed with status code: {}"
                                 .format(template, dir_name, request.status_code))
                    continue
                else:
                    _logger.error("\nAborting on {} {} with status code: {}"
                                  .format(template, dir_name, request.status_code))
                    sys.exit(1)

        else:
            _logger.debug("\nTemplate {} {} already downloaded: {}"
                          .format(template, dir_name, template))


def store_zip(template_root, zip_name, request, dir_name):
    """Saves and unpacks .zip at `template_root`.

    """
    dir_path = template_root + dir_name + '/'
    zip_path = template_root + zip_name + ".zip"

    with open(zip_path, "wb") as temp_zip:
        request.raw.decode_content = True
        shutil.copyfileobj(request.raw, temp_zip)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    zip_ref = zipfile.ZipFile(zip_path, "r")
    zip_ref.extractall(dir_path)
    zip_ref.close()



def parse_args(args):
    """ Parse command line parameters.

    :return: command line parameters as :obj:`airgparse.Namespace`
    Args:
        args ([str]): List of strings representing the command line arguments.

    Returns:
        argparse.Namespace: Simple object with a readable string
        representation of the argument list.

    """
    parser = argparse.ArgumentParser(
        description="Download all website templates from Pixelarity.")

    parser.add_argument(
        '-f',
        '--force',
        help="Overwrite all themes already downloaded",
        action='store_true')
    parser.add_argument(
        '-p',
        '--prefix',
        nargs=1,
        type=str,
        default="pixelarity",
        help="File to store template downloads in",
        action='store')

    return parser.parse_args(args)


def setup_logs():
    """ Set up logger to be used between all modules.

    Set logging root and file handler configuration to default to
    ``DEBUG`` and write output to ``main.log``. Set console
    handler to default to ``ERROR``.

    """
    logging.basicConfig(level=logging.DEBUG, filename='../calibration.log',
                        filemode='w')
    _logger.setLevel(logging.DEBUG)

    # create file handler which logs messages
    fh = logging.FileHandler('../calibration.log')
    fh.setLevel(logging.DEBUG)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    # add the handlers to the logger
    _logger.addHandler(fh)
    _logger.addHandler(ch)


if __name__ == "__main__":
    start = time.time()
    main(sys.argv[1:])
    print("[*] Downloaded templates in {} minutes"
          .format((time.time() - start) // 60))
    sys.exit(0)
